IMAGE_TAG="dashkube"
CONFIG_FILE="config.yaml"
GEN_DIR="generated"

# Build the docker image with the configured image tag.
.PHONY: image
image:
	docker build -t ${IMAGE_TAG} .

# Package the helm chart into a usable tgz file.
.PHONY: chart
chart:
	helm package helm

# Locally autogenerate the files based on a config file once.
# Please note that config file needs to be created.
.PHONY: local-gen
local-gen:
	python3 src/generate.py --config-file ${CONFIG_FILE} --output-dir ${GEN_DIR}

# Locally run the autogenerator and the server. 
# Please note that the config file needs to be created.
# Additionally, the environment should have kubectl access.
.PHONY: local-run
local-run:
	python3 src/generate.py --config-file ${CONFIG_FILE} --output-dir ${GEN_DIR}
	python3 src/server/server.py
