# Helpful utilities that can be used commonly by both the code generator
# and server-side code.
#
# Author: Ardalan Ahanchi
# Date: December 2023

import sys
import time
import re

from kubernetes import client, config, dynamic
from kubernetes.dynamic import DynamicClient
from kubernetes.dynamic.resource import Resource

# Load the kubernetes configuration depending on if it's in cluster or not.
# This is used for both generator and server programs to allow kube access.
try:
    config.load_kube_config()
except:
    config.load_incluster_config()

# Setup the dynamic client for API calls. This is used for both generator
# and server programs to manipulate custom (or core) resources.
dyn_client: DynamicClient = DynamicClient(client.api_client.ApiClient())

# Setup the API for getting information about the CRDs.
crd_api: Resource = dyn_client.resources.get(
        api_version="apiextensions.k8s.io/v1",
        kind="CustomResourceDefinition")

# Simple wrapper for the dynamic client to allow getting the API
# for a spefieid resource. For resources in the core API, please
# pass "core" as the group name.
def get_resource_api(group: str, version: str, kind: str) -> Resource:
    # If group is core, set it to empty. Otherwise, use api/version
    api_version = version if group == "core" else f"{group}/{version}"

    # Return the dynamic client API for the resource.
    return dyn_client.resources.get(api_version=api_version, kind=kind)

# Prints an error message to stderr, and exits the program
# with a non-ok error code.
def error(msg: str) -> None:
    print(f"Error: {msg}", file=sys.stderr)
    sys.exit(1)

# Call to represent a full CRD as present in the generator.
# This includes initialization of the CRD and contains basic
# information about it. Moreover, it can keep track of the
# current storage version and store the schema to be used.
class CrdInfo:
    # Initializer which stores the name and defines basic fields.
    def __init__(self, crd_name: str):
        self.name: str = crd_name
        self.res_version: int = 0
        self.kind: str = ""
        self.group: str = ""
        self.version: str = ""
        self.schema: dict = {}
        self.printer_cols: list = []

    # Function to sync the basic CRD info with the cluster. This will update
    # the schema and other fields if the CRD in the cluster was updated.
    # It returns True if it detects a change in the CRD. In such cases,
    # the internal state of the object will be updated. If the CRD
    # remained the same since the last call or if the CRD was not
    # found in the cluster, it will return False.
    def sync_and_check(self, omitted_printer_cols: set = set()) -> bool:
        # Try to get the CRD from the CRD API and turn into a dictionary.
        try:
            parsed_crd: dict = crd_api.get(name=self.name).to_dict()
        except Exception as e:
            print(f"Warning: Could not fetch the {self.name} CRD: {e}")
            return False

        # Get the required information from the CRD.
        try:
            # Check if the current resource version is matching the current CRD.
            # If so, exit since we are still up to date.
            if self.res_version == parsed_crd["metadata"]["resourceVersion"]:
                return False
            # Otherwise, update the res_version field.
            else:
                self.res_version = parsed_crd["metadata"]["resourceVersion"]

            self.kind: str = parsed_crd["spec"]["names"]["kind"]
            self.group: str = parsed_crd["spec"]["group"]

            # Find the first storage version and use that.
            for ver in parsed_crd["spec"]["versions"]:
                # If version is served and storage.
                if ver["served"] and ver["storage"]:
                    # Set the schema and version as current version's info.
                    self.schema = ver["schema"]["openAPIV3Schema"]["properties"]
                    self.version = ver["name"]

                    # Check if we have additional printer columns.
                    if "additionalPrinterColumns" in ver:
                        # If so, first reset the printer columns stored.
                        self.printer_cols = []

                        # Then go through the printer cols.
                        for col in ver["additionalPrinterColumns"]:
                            # If not omitted, add it to final.
                            if col["name"] not in omitted_printer_cols:
                                self.printer_cols.append(col)
        except Exception as e:
            error(f"Could not parse fields in the {nickname} CRD:\n{e}")

        # If we get here, CRD info were updated, thus return True.
        return True

# Function to load a file template and return the contents
# with the template keywords replaced as directed. This can 
# be used for reading scripts or forms from specific files.
# The templetized items should be used as "{{name}}".
# 
# Parameters
# `filename`: Name of the file to be read.
# `kwargs`: Parameterized arguments for replacements.
#
# Returns
# Loaded file's string representation with fields replaced.
def load_template(filename: str, **kwargs) -> str:
    # Try to open the input file and read the contents.
    try:
        # Open the file and read the data into memory.
        with open(filename) as input_file:
            contents = input_file.read()
    except Exception as e:
        error(f"Could not open the template file at {filename}.")

    # Go through the args in order to templatize.
    for name, value in kwargs.items():
        # Replace {{ template name }} with the value.
        contents = re.sub("{{[ \t]*" + name + "[ \t]*}}", value, contents)

    return contents
