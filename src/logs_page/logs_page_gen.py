# Script to generate a logs page which can display logs for a 
# specific pod. It allows switching between containers and 
# viewing logs for the desired container.
#
# Author: Ardalan Ahanchi
# Date: December 2023 

import argparse
import yaml
import copy
import pprint
import sys

sys.path.append('src')
from utils import *


# Function that is called by the main generator. It puts together
# all the files and adds the needed fields in runtime.
#
# Parameters
# `output_file`: Path to the output file. It will be created if not present.
# `logo_path`: Path to the logo file which is used for page header.
def generate(output_file: str, logo_path: str):
    # For the final output results.
    out: str = ""

    out +=  "<!DOCTYPE html>\n"
    out +=  "<html>\n"
    out +=  "<body onload='init_page()'>\n"

    # Add the logo and title with a link to index page.
    out +=  "<div class='page_header'>\n"
    out += f"<a href='/'><img src='{logo_path}' class='page_header_logo'></a>\n"
    out +=  "<h1 class='page_header_title'>Logs for {{pod_name}}</h1>\n"
    out +=  "</div>\n"

    # Add a select and a label for it.
    out +=  "<div class='container_selector'>\n"
    out +=  "<label for='containers'>Container</label>\n"
    out +=  "<select name='containers' id='containers' class='containers' "
    out +=      "title='Container selector for the logs output' "
    out +=      "onchange='select_logs(this.value)'></select>\n"
    out +=  "</div>\n"

    out +=  "<div id='all_logs'></div>\n"

    # Load the script for fetching the logs.
    out +=  "<script>\n"
    out +=  load_template("src/logs_page/logs_page.tmpl.js")
    out +=  "</script>\n"

    # Load the common and specific CSS style.
    out +=  "<style>\n"
    out +=  load_template("src/common_style.tmpl.css")
    out +=  load_template("src/logs_page/style.tmpl.css")
    out +=  "</style>\n"

    # Add the page footing.
    out +=  "</body>\n"
    out +=  "</html>\n"

    # Try to open the output file and write results to it.
    try:
        with open(output_file, "w") as output:
            output.write(out)
    except Exception as e:
        error(f"Could not write the results to the {output_file} file.")


