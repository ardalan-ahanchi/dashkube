// This file defines the functionality needed to get the logs for 
// all containers of a given pod.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// Initialize the page, get containers list, and choose first container.
async function init_page() {
    // Get the pod definition in the namespace.
    const response = await fetch(location.protocol + "//" + 
            location.host + "/resource/core/v1/Pod/namespaces/{{namespace}}/{{pod_name}}", {
        method: "GET",
    });  

    // Check the response.
    if (response.ok) {
        response.text().then(async (value) => {
            // Parse the results.
            let parsed = JSON.parse(value);
           
            // Get the containers select.
            let select = document.getElementById("containers");

            // Get the Div for all the logs.
            let all_logs = document.getElementById("all_logs");

            // Go through containers.
            parsed.status.containerStatuses.forEach((container) => {
                // Populate the select.
                select.options.add(new Option(container.name, container.name));
                
                // Add the paragraph.
                all_logs.innerHTML += "<p id='" + container.name + "_logs'></p>";
            
                // Get the logs for the container.
                get_logs(container.name);
            });

            // Make the target container visible.
            select_logs(select.value);
        });
    // Alert the user if failed to get logs.
    } else {
        response.text().then((value) => {
            alert("Could not get the containers list:\n" + value);
            history.back();
        });
    }
}

// Function to get logs and read from a log stream for a container.
// This then populates the correct UI element with the logs.
async function get_logs(pod_container) {
    // Get the pods info from the server.
    const response = await fetch(location.protocol + "//" + location.host + 
            "/stream-logs/{{namespace}}/{{pod_name}}/" + pod_container, {
        method: "GET",
    });

    // Check the response.
    if (response.ok) {
        const reader = response.body.getReader();

        // Get the containers select.
        let select = document.getElementById("containers");

        // Get the logs paragrarph.
        let logs = document.getElementById(pod_container + "_logs");

        // Clear the logs before getting it from the stream.
        logs.innerText = "";
        
        let done, value;
        while (!done) {
            try {
                // Read the logs from the stream.
                ({ value, done } = await reader.read());
                if (done) {
                    return;
                }

                // Decode the text and add it to the UI.
                logs.innerText += new TextDecoder().decode(value);
            } catch(err) {
                return;
            }
        }
    } else {
        // Send an allert based on the server's response.
        response.text().then((value) => {alert("Could not subscribe to logs:\n"+ value)});
        history.back();
    }
}

// Function to only make the logs for the selected container visible in the logs.
function select_logs(pod_container) {
    // Get the div with all the logs.
    let all_logs = document.getElementById("all_logs");

    // Go through every child and set display to none.
    for (const log of all_logs.children) {
        // Check if we have the current container.
        if (log.id === (pod_container + "_logs")) {
            // If so, make sure the logs are displayed.
            log.style.display = "block";
        } else {
            // Otherwise, hide the logs.
            log.style.display = "none";
        }
    }
}
