// This file defines the list operations. This can apply to any list within the form.
// It allows adding and deleting from these lists. Additionally, the relevant IDs
// will be added to the IDs array.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// Function to add to a list with a given identifier. The new item will
// be created from the template and added to the list. Additionally, the 
// IDs array will be populated as expected.
function list_add(identifier) {
    var li = document.createElement("LI");
    var tmp = document.getElementById(identifier + "{{id_seperator}}fieldset{{id_seperator}}template");
    var clone = tmp.content.cloneNode(true);
    var list = document.getElementById(identifier + "{{id_seperator}}items");
    clone.id = identifier + "{{id_seperator}}" + list.children.length;

    // Remove the specific list indeces, and then find the index_kw based on provided map.
    var id_sections = identifier.split("{{id_seperator}}");
    for (let i = 0; i < id_sections.length; i++) {
        if (/^\d+$/.test(id_sections[i])) {
            id_sections[i] = "{{index_keyword}}";
        }
    }
    var index_kw = list_id_to_kw[id_sections.join("{{id_seperator}}")];

    // Go through all the fields containing the index in the ID.
    // then replace the index placeholder with the actual item index.
    var all_items = clone.querySelectorAll("[id*=" + index_kw + "]");
    for (let i = 0; i < all_items.length; i++) {
        all_items[i].id = all_items[i].id.replace(index_kw, list.children.length);
        if (all_items[i].tagName == "BUTTON" || all_items[i].tagName == "TEMPLATE"){
            all_items[i].outerHTML = all_items[i].outerHTML.replace(index_kw, list.children.length);
        }
        if (all_items[i].tagName == "INPUT" || all_items[i].tagName == "SELECT"){
            ui_all_ids.push(all_items[i].id);
        }
    }
    list.appendChild(clone);
}

// Function to delete from a list with a given identifier. The removed
// IDs in the IDs array will be ignored on application time.
function list_del(identifier) {
    li = document.getElementById(identifier + "{{id_seperator}}items");
    elements = li.children;
    if (elements.length > 0) {
        li.removeChild(elements[elements.length - 1]);
    }
}
