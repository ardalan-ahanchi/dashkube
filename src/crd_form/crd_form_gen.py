# Script to generate a (mostly) static HTML page for a given CRD solely based 
# on the CRD schema. It requires an input and output file to be passed to it.
# for the list of arguments please run the script with the `--help` argument.
#
# Author: Ardalan Ahanchi
# Date: December 2023 

import argparse
import yaml
import copy
import pprint
import sys
import re

sys.path.append('src')
from utils import *

ID_SEPERATOR: str = "__"
INDEX_KEYWORD: str = "LISTINDEX"
NULL_KEYWORD: str = "UNSPECIFIED"

__index_keyword_count: int = 0

# Convert a chain to an ID. For instance [a, b] would become a__b if
# the ID separator is specified as "__".
def __chain_to_id(chain: list, seperator: str = ID_SEPERATOR) -> str:
    identifier = ""
    for idx, name in enumerate(chain):
        identifier += (name if idx == 0 else f"{seperator}{name}")
            
    return identifier

# Convert and ID to chain list. For instance a__b would become [a, b]
# if the ID seperator is specified as "__".
def __id_to_chain(identifier: str, seperator: str = ID_SEPERATOR) -> list:
    return identifier.split(seperator)

# Main conversion function which recursively goes through the CRD schema and generates
# the required HTML and JS code.
def __convert_data(data: dict, name: str, chain: list, omit_label: bool = False,
        is_template: bool = False, references: dict = {}) -> str:
    # If we don't have a type anymore, simply return.
    if "type" not in data:
        return ""

    # To store the optional title (for description).
    title: str = ""
    if "description" in data:
        description = data["description"].replace("\"", "&quot;")
        title = f"title=\"{description}\""

    # To store the out.
    out: str = ""

    # Get the identifier based on the chain and current element.
    identifier = __chain_to_id(chain + [name])

    # If we have an object.
    if data["type"] == "object":
        out += f'<fieldset id="{identifier}{ID_SEPERATOR}fieldset">\n'
        
        # Only print legend if this is not a list element.
        if not omit_label and not is_template:
            out += f'<legend {title}>{name}</legend>\n'

        # Go through the keys in proprties.
        for key in data["properties"]:
            # Convert them recursively.
            out += __convert_data(data["properties"][key], key,
                    chain + [name], False, is_template, references)

        out +=  '</fieldset>\n'
    elif data["type"] == "array":
        global __index_keyword_count
        index_keyword = f"{INDEX_KEYWORD}{__index_keyword_count}"
        __index_keyword_count += 1

        out += f'<fieldset id="{identifier}{ID_SEPERATOR}fieldset">\n'
        
        # If we have a solid name (not a nested list), add legend.
        if INDEX_KEYWORD not in name:
            out += f'<legend {title}>{name}</legend>\n'

        # Add to list_id_to_kw array which will be used for editing or adding to the list.
        # please note that this id does not include the unique list index.
        id_no_unique_kw = re.sub(f"{INDEX_KEYWORD}[0-9]+", INDEX_KEYWORD, identifier)
        out += f'<script>list_id_to_kw["{id_no_unique_kw}"] = "{index_keyword}"</script>\n'

        out += f'<ul id="{identifier}{ID_SEPERATOR}items">\n'
        out += f'</ul>\n'
        out += f'<template id="{identifier}{ID_SEPERATOR}fieldset{ID_SEPERATOR}template">\n'
        out += f'<div>\n'

        # Print one item as an object.
        out += __convert_data(data["items"], index_keyword,
                chain + [name], True, True, references)

        out += '</div>\n'
        out += '</template>\n'
        
        list_id = f"{identifier}{ID_SEPERATOR}items"
    
        # Add a button for adding to a list.
        out += f'<button type="button", id="{identifier}{ID_SEPERATOR}addbutton", onClick="list_add(\'{identifier}\')", class="list_button">+</button>\n'
    
        # Add a button for deleting from a list.
        out += f'<button type="button", id="{identifier}{ID_SEPERATOR}delbutton", onClick="list_del(\'{identifier}\')", class="list_button">-</button>\n'

        out +=  '</fieldset>\n'
    elif data["type"] == "string":
        # Keep track of if this field is a reference.
        is_ref: bool = False
        ref_crd: dict = {}

        # If there are any references configured, check the value of ref.
        if len(references) != 0:
            # Reformat the identifier to see if have a match with the field.
            ref_check: str = "." + re.sub(f"{ID_SEPERATOR}{INDEX_KEYWORD}[0-9]+", "[:]",
                    identifier).replace(ID_SEPERATOR, ".")

            # Set the value of is_ref and ref_crd accordingly.
            if ref_check in references:
               is_ref = True
               ref_crd = references[ref_check]

        # If we have a reference, add a div.
        if is_ref:
            out +=  '<div class="reference_input">\n'

        # Add label only if not a list element.
        if not omit_label:
            out += f'<label for="{identifier}" {title}>{name}</label>\n'

        # If we have an enum, put a drop-down.
        if "enum" in data:
            value = f' ({data["default"]})' if "default" in data else ""
            out += f'<select id="{identifier}">\n'
            out += f'<option disabled selected value={NULL_KEYWORD}>select{value}</option>\n'
            for opt in data["enum"]:
                out += f'<option value={opt}>{opt}</option>\n'
            out += f'</select><br>\n'
        # Otherwise, put a string field.
        else:
            value = data["default"] if "default" in data else ""
            opts = ""

            # If we have a reference, add handlers and a div.
            if is_ref:
                opts += ('onfocus="refresh_references(this, \'' +
                        ref_crd["api_version"] +  '\', \'' + ref_crd["kind"] + '\')"' if is_ref else '')

            out += (f'<label {title}><input type="text" id="{identifier}" ' +
                    f'name="{name}" placeholder="{value}" {opts}></label><br>\n')

            # If we have a reference, add a link to manage page.
            if is_ref:
                out += ('<a href="/manage/' + ref_crd["name"] + '/namespaces/{{namespace}}'
                        '" target="_blank" class="reference_manage">Manage ' +
                        ref_crd["nickname"] + '</a>\n')

        # If this was a reference, close the div.
        if is_ref:
            out +=  '</div>\n'

    elif data["type"] == "integer":
        # Add label only if not a list element.
        if not omit_label:
            out += f'<label for="{name}" {title}>{name}</label>\n' 
        
        # Add minimum and maximum options if needed.
        options = ""
        if "minimum" in data:
            options += f' min={data["minimum"]}'
        if "maximum" in data:
            options += f' max={data["maximum"]}'
        if "default" in data:
            options += f' placeholder="{data["default"]}"'

        # Print the input type.
        out += f'<input type="number" id="{identifier}" name="{name}"{options}><br>\n'
    elif data["type"] == "boolean":
        # Add label only if not a list element.
        if not omit_label:
            out += f'<label for="{name}" {title}>{name}</label>\n' 
        
        # Add default option to yes/no if specified.
        value = f' ({"yes" if data["default"] else "no"})' if "default" in data else ""
        out += f'<select id="{identifier}">\n'
        
        # Add the options in addtion to the default option.
        out += f'<option disabled selected value={NULL_KEYWORD}>select{value}</option>\n'
        out +=  '<option value=true>yes</option>\n'
        out +=  '<option value=false>no</option>\n'
        out += f'</select><br>\n'

    # If we have concrete types, add the ID to the list of all IDs.
    if data["type"] in ["string", "integer", "boolean"] and not is_template:
        out +=  '<script>\n'
        out += f'ui_all_ids.push("{identifier}")\n'
        out +=  '</script>\n'

    # Return the output of this run.
    return out

# Main generation script. It requires some information about the CR and the schema,
# it then calls the required recursive function to output the generated files.
def __generate_form(schema: dict, api_version: str, kind: str, nickname: str,
        omitted_keys: set, references: dict, logo_path: str) -> str:
    # For the final output.
    out: str = ""

    # Define the boilerplate definitions and title.
    out +=  '<!DOCTYPE html>\n'
    out +=  '<html>\n'
    out +=  '<body>\n'

    # Add the logo and title with a link to index page.
    out +=  "<div class='page_header'>\n"
    out += f"<a href='/'><img src='{logo_path}' class='page_header_logo'></a>\n"
    out += f"<h1 id='page_title' class='page_header_title'>New {nickname}</h1>\n"
    out +=  "</div>\n"
    
    # Define a templatized variable to be used for editing.
    out +=  '<script>is_editing = "{{edit_resource_name}}";</script>\n'
    
    # Initialize the list of IDs (for all the fields). 
    out +=  '<script>ui_all_ids = [];</script>\n'

    # Initialize the list id to index keyword map.
    out +=  '<script>list_id_to_kw = {};</script>\n'

    # Add a div for the whole form.
    out += '<div class="crd_form">\n'

    # Add the name field for the CR.
    name_field_id = f"metadata{ID_SEPERATOR}name"
    out += f'<label for="{name_field_id}">name</label>\n'
    out += f'<label><input type="text" id="{name_field_id}" name="name" class="name_field"></label><br>\n'

    # Go through the top-level keys in schema.
    for key in schema:
        # If the key is not an omitted key, convert the data.
        if key not in omitted_keys:
            out += __convert_data(schema[key], key, [], False, False, references)

    # Load scripts that are needed for various functionality.
    out +=  '<script>\n'
    
    # Load the list operations script to allow adding/deleting to/from lists.
    out +=  load_template("src/crd_form/list_operations.tmpl.js",
                id_seperator=ID_SEPERATOR,
                index_keyword=INDEX_KEYWORD)

    # Load the apply config script to allow sending configuration to server.
    out +=  load_template("src/crd_form/apply_config.tmpl.js",
                api_version=api_version,
                kind=kind,
                name_field_id=name_field_id,
                null_keyword=NULL_KEYWORD)

    # Load the edit config script to allow editing existing objects.
    out +=  load_template("src/crd_form/edit_config.tmpl.js",
                nickname=nickname,
                name_field_id=name_field_id,
                api_version=api_version,
                kind=kind,
                id_seperator=ID_SEPERATOR)

    # Load the referenced resource script to allow soft references.
    out +=  load_template("src/crd_form/references.tmpl.js",
                id_seperator=ID_SEPERATOR)

    out +=  '</script>\n'

    # Load common and specific styling to make the page pretty.
    out +=  "<style>\n"
    out +=  load_template("src/common_style.tmpl.css")
    out +=  load_template("src/crd_form/style.tmpl.css")
    out +=  "</style>\n"

    # Add the button to apply the configuration.
    out +=  '<button type="button" onClick="apply_config()" class="standalone_button">Apply</button>\n'
    
    out +=  '</div>\n'
    out +=  '</body>\n'
    out +=  '</html>\n'

    return out


# Function that is called by the main generator. It constructs the
# references, calls the lower layer generator function, and
# writes the output to the specified file.
#
# Parameters
# `crd_info`: Map of CRD names to their information.
# `schema`: Dictionary containing the full parsed CRD schema.
# `api_version`: The full api_version string for the CRD.
# `kind`: The kind of the CRD being autogenerated.
# `nickname`: Nickname that can be used for the CRD.
# `ommited_keys`: Set of top-level keys that will be omitted from codegen.
# `cfg_references`: The references configuration from this CRD to others.
# `output_file`: Path to the output file. It will be created if not present.
# `logo_path`: Path to the logo file used in the header.
def generate(schema: dict, api_version: str, kind: str,
        nickname: str, omitted_keys: set, cfg_references: list,
        output_file: str, logo_path: str, crd_info: dict):
    # Go through the cfg_references and constrct references as key-value pairs.
    references: dict = {}
    for cfg_ref in cfg_references:
        references[cfg_ref["field"]] = {
            "name": cfg_ref["to"],
            "nickname": cfg_ref["nickname"],
            "api_version": crd_info[cfg_ref["to"]].group + "/" + crd_info[cfg_ref["to"]].version,
            "kind": crd_info[cfg_ref["to"]].kind
        }

    # Run the generate function with the parsed_crd values.
    final_out: str = __generate_form(schema, api_version, kind,
            nickname, omitted_keys, references, logo_path)

    # Try to open the output file and write results to it.
    try:
        with open(output_file, "w") as output:
            output.write(final_out)
    except Exception as e:
        error(f"Could not write the results to the {output_file} file.")
