// This file defines the functions needed for refreshing the 
// referenced resources. This is to allow refreshing the 
// drop-down list when the user first clicks it.
//
// Author: Ardalan Ahanchi
// Date: January 2024

// Function that is called once the input is focused.
// This function fetches the resource names from the 
// server and updates the input with the correct values.
async function refresh_references(input, api_version, kind) {
    // Make the request to the server with the given CRDs.
    const response = await fetch(location.protocol + "//" + location.host + "/resource/" + api_version + "/" + kind + "/namespaces/{{namespace}}", {
        method: "GET",
    });

    // Check if we were successful.
    if (response.ok) {
        response.text().then((value) => {
            // Parse the results.
            var parsed = JSON.parse(value);

            // Construct the ID of the datalist based on ID of input.
            var datalist_id = input.id + "{{FIELD_SEPEATOR}}datalist"

            // Try to find the datalist element by ID.
            var dl = document.getElementById(datalist_id);
            
            // Create a new datalist element if not found. 
            if (dl == null) {
                dl = document.createElement("DATALIST");
                dl.id = datalist_id;
                input.appendChild(dl);
            // Otherwise, clear the options for the datalist.
            } else {
                dl.innerHTML = "";
            }

            // Make sure the input is pointing to datalist.
            input.setAttribute("list", datalist_id);
            
            // Go through the resources returned from the server.
            for (let i = 0; i < parsed.items.length; i++) {
                // Create an option for datalist and append to it.
                var option = document.createElement("OPTION");
                option.value = parsed.items[i].metadata.name;
                dl.appendChild(option);
            }
        });
    } else {
        // Send an allert based on the server's response.
        response.text().then((value) => {alert(value)});
    }
}
