// This file defines the functionality that is needed to gather 
// information from the page and sending the config to the 
// server. It then must display the results.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// Function that is called when config needs to be applied.
// It reads the data from the fields specified in IDs array and 
// sends a request to the server. It then shows the results.
async function apply_config() {
    // To store the final results.
    var final_dict = {}
    
    // Add apiVersion and kind fields.
    final_dict["apiVersion"] = "{{api_version}}";
    final_dict["kind"] = "{{kind}}";
     
    // Get the name field and add name to final results. 
    var name_field = document.getElementById("{{name_field_id}}");
    if (name_field == null) {
        alert("The resource name can not be empty.");
    } else {
        final_dict["{{name_field_id}}"] = name_field.value;
    }
    
    // Go through all the IDs that we stored for checking out.
    for (let i = 0; i < ui_all_ids.length; i++ ) {
        // Get the element for the specific ID.
        element = document.getElementById(ui_all_ids[i]);
        // Check if element was found for the ID (in case it was deleted).
        value = element == null ? "" : element.value;
        // Check if the element is not the default/unset value.
        if (value != "" && value != "{{null_keyword}}") {
            final_dict[ui_all_ids[i]] = value;
        }
    }

    // Send the key-value pairs to the server.
    const response = await fetch(location.protocol + "//" + location.host +
            "/flat-resource/{{api_version}}/{{kind}}/namespaces/{{namespace}}", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(final_dict)
    })

    // Send an alert with the results.
    alert(await response.text());

    // Go to the original (probably manage) page if successful.
    if (response.ok) {
        history.back();
    }
}
