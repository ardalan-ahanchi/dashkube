// This file defines the functionality needed to edit the form. It retrieves
// the requested resource from the server and populates the forms accordingly.
// It allows the user to modify the form and re-apply the new configuration.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// Call the load function to populate the fields.
if (is_editing !== "") {
    load_to_edit(is_editing);
}

// If we are editing, this function will be called to retrieve object and 
// populate the fields. The name of the object will be passed here.
async function load_to_edit(name) {
    // Set the title and disable name since we are editing now.
    document.getElementById("page_title").innerText = "Edit {{nickname}}";
    var name_field = document.getElementById("{{name_field_id}}");
    name_field.disabled = true;
    name_field.value = is_editing;

    // Make a request to the server to get the configuration in the expected format.
    const response = await fetch(location.protocol + "//" + location.host + "/flat-resource/{{api_version}}/{{kind}}/namespaces/{{namespace}}/" + name, {
        method: "GET",
    });

    // If Request is alright, parse the data, and iterate over the keys.
    if (response.ok) {
        response.text().then((value) => {
            var parsed = JSON.parse(value);
            for (const [key, value] of Object.entries(parsed)) {
                var splitted = key.split("{{id_seperator}}");
                // Go through the sections of the key in order to add array items (if necessary).
                for (let i = 0; i < splitted.length; i++) {
                    // If we have just a number, we can assume it is an index into an array.
                    if (/^\d+$/.test(splitted[i])) {
                        // Construct the list_id up to this point.
                        list_id = splitted.slice(0, i).join("{{id_seperator}}");

                        // Get the list based on ID and add to list for the delta in count.
                        list = document.getElementById(list_id + "{{id_seperator}}items");
                        curr_len = list === null ? 0 : list.children.length;
                        for (let j = 0; j < (parseInt(splitted[i]) - curr_len + 1); j++) {
                            list_add(list_id);
                        }
                    }
                }

                // Get the current field and set the value for it.
                var curr_field = document.getElementById(key);
                if (curr_field !== null) {
                    curr_field.value = value;
                }
            }
        })
    } else {
        alert("Could not retrieve requested resource.");
        history.back();
    }
}
