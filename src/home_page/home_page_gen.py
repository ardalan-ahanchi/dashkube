# Script to generate a (mostly) static HTML home page for the list of CRDs.
# It allows adding categories to the CRD with the link to the CRD managers.
# It additionally has  the option to scale the required deployments and to
# watch specific pods for status and logs.
#
# Author: Ardalan Ahanchi
# Date: December 2023 

import argparse
import yaml
import copy
import pprint
import sys

sys.path.append('src')
from utils import *

# Function to generate the sidebar components based on the passed CRDs.
# This includes categorized expandable elements that contain specific config.
#
# Parameters
# `crds_config`: Parsed CRD section of the configuration file.
#
# Returns
# The generated HTML contents with Javascript loaded.
def __generate_config_menu(crds_config: list) -> str:
    # For storing the final results.
    out: str = ""

    # Create a map between category and CRDs under it.
    category_to_crds: dict = {}
    for crd_cfg in crds_config:
        # Get category and initialize in map if needed.
        category: str = crd_cfg["category"] if "category" in crd_cfg else ""
        if category not in category_to_crds:
            category_to_crds[category] = []

        # Add the current CRD to this category.
        category_to_crds[category].append(crd_cfg)
    
    # Define a div for the whole menu.
    out +=  "<div class='config_menu'>\n"

    # Go through teh categories.
    for category in category_to_crds:
        # Add an expandable button if there is a category.
        if category != "":
            out += f"<button type='button' class='category' onclick='trigger_category(this)'>{category}</button>\n"
            out +=  "<div class='crd_list'>\n"
       
        # Go through the CRDs and add a button for each.
        for crd_cfg in category_to_crds[category]: 
            cr_manager_path = '"/manage/' + crd_cfg['name'] + '/namespaces/" + get_namespace()'
            out += f"<button type='button' onclick='location.href={cr_manager_path}' class='crd_button'>{crd_cfg['nickname']}</button>\n"

        # Close the div for categorized CRDs.
        if category != "":
            out +=  "</div>\n"

    # Close the div for the config menu.
    out +=  "</div>\n"

    # Load the JS code.
    out +=  "<script>\n"
    out +=  load_template("src/home_page/config_menu.tmpl.js")
    out +=  "</script>\n"

    # Return the final results.
    return out

# Function the generate the external link buttons based on the
# passed external links configuration.
#
# Parameters
# `external_links_config`: The parsed ext links section of config.
#
# Returns
# The generated HTML contents with Javascript loaded.
def __generate_external_links(external_links_config: list) -> str:
    # For storing the final results.
    out: str = ""

    # Define a div for the whole section.
    out += "<div class='external_links'>"

    # Go through the external links config and add buttons.
    for cfg in external_links_config:
        nickname = cfg["nickname"]
        link = '"' + cfg["link"] + '"'
        description = '"' + cfg["description"] + '"' if "description" in cfg else '""'
        out += (f"<button type='button' onclick='window.open({link}, \"_blank\")' " +
                f"title={description} class='ext_link'>{nickname}</button>\n")

    # Close the div for the config menu.
    out +=  "</div>\n"

    # Return the final results.
    return out

# Function to generate the list of pods with the option to
# view logs for each pod.
#
# Parameters
# `watched_pods": List of watched pods based on config schema.
# `refresh_interval`: Time between getting updated information from the server.  
#
# Returns
# The generated HTML contents with Javascript loaded.
def __generate_watched_pods(watched_pods: list, refresh_interval: int) -> str: 
    # For storing the final results.
    out: str = ""

    # Define a div for the whole menu.
    out +=  "<div class='wm_element'>\n"
    out +=  "<h2 class='wm_heading'>Pods</h2>\n"

    # Add the list which will be used for the pods.
    out +=  "<ul id='pods_list' class='wm_element_list'></ul>\n"

    # Construct the regex filter that is used for watching the pods.
    pods_pattern: str = ""
    for idx, pod in enumerate(watched_pods):
        pods_pattern += "" if idx == 0 else "|"
        pods_pattern += pod["pattern"]

    # Close the div for the config menu.
    out +=  "</div>\n"

    # Load the template for the list elements.
    out +=  load_template("src/home_page/watched_pods.tmpl.html",
                pod_buttons="")

    # Load the JS code.
    out +=  "<script>\n"
    out +=  load_template("src/home_page/watched_pods.tmpl.js",
                pods_pattern=f"/{pods_pattern}/",
                refresh_interval=str(refresh_interval))
    out +=  "</script>\n"

    # Return the final results.
    return out

# Function to generate the list of scalable deployments which would 
# allow scaling the desired deployment.
#
# Parameters
# `scalables": List of scalable components based on config schema.
# `refresh_interval`: Time between getting updated information from the server.  
#
# Returns
# The generated HTML contents with Javascript loaded.
def __generate_scaling(scalables: list, refresh_interval: int) -> str: 
    # For storing the final results.
    out: str = ""

    # Define a div for the whole menu.
    out +=  "<div class='wm_element'>\n"
    out +=  "<h2 class='wm_heading'>Scaling</h2>\n"
    
    # Load the template for the list element.
    out += load_template("src/home_page/scaling.tmpl.html")
 
    # Add the list which will be used for the scaling.
    out +=  "<ul id='scaling_list' class='wm_element_list'></ul>\n"

    # Close the div for the scaling menu.
    out +=  "</div>\n"

    # Create a list of valid type/names.
    valid_type_names: str = ""
    nickname_map: str = ""
    min_replicas_map: str = ""
    max_replicas_map: str = ""
    for scalable in scalables:
        curr_type_name = "'" + scalable["type"] + "/" + scalable["name"] + "'"

        # Add type/name to the string.
        valid_type_names += curr_type_name + ", "

        # Add the map of typename to nickname to second string.
        nickname_map += curr_type_name + ": '" + scalable["nickname"] + "', "
        # Add the map of min/max replicas.
        min_replicas_map += curr_type_name + ": " + str(scalable["minReplicas"]) + ", "
        max_replicas_map += curr_type_name + ": " + str(scalable["maxReplicas"]) + ", "


    # Load the JS code.
    out +=  "<script>\n"
    out +=  load_template("src/home_page/scaling.tmpl.js",
                refresh_interval=str(refresh_interval),
                valid_type_names=valid_type_names,
                nickname_map=nickname_map,
                min_replicas_map=min_replicas_map,
                max_replicas_map=max_replicas_map)
    out +=  "</script>\n"

    # Return the final results.
    return out

# Function that is called by the main generator. It gets the required fields, 
# from the parsed CRD, and then calls the internal generate function
# with the correct parameters. It then writes the results to the output file.
#
# Parameters
# `config`: Parsed configuration file used for auto generation.
# `output_file`: Path to the output file. It will be created if not present.
def generate(config: dict, output_file: str):
    # For the final output results.
    out: str = ""

    # Start the file and add onload functions.
    out +=  "<!DOCTYPE html>\n"
    out +=  "<html>\n"
    out +=  "<body onload='refresh_pods(); refresh_scaling();'>\n"

    # Add the logo, title, and namespace selector.
    out +=  "<div class='page_header'>\n"
    out +=  "<img src='" + config["logoPath"] + "' class='page_header_logo'>\n"
    out += f"<h1 class='page_header_title'>{config['title']}</h1>\n"

    # Load the code to handle namespace and give first one as default.
    out +=  "<script>"
    out +=  load_template("src/home_page/namespace.tmpl.js", default_namespace=config["namespaces"][0])
    out +=  "</script>"

    # Add a select to choose between namespaces if there are more than one.
    if len(config["namespaces"]) > 1:
        out +=  "<div class='page_header_ns'>\n"
        out +=  "<label for='ns_select' class='page_header_ns_label'>Namespace</label>\n"
        out +=  ("<select id='ns_select' class='page_header_ns_select' " +
                "onchange='set_namespace(this.value);'>\n")
        for ns in config["namespaces"]:
            out += f"<option value='{ns}'>{ns}</option>\n"
        out +=  "</select>\n"
        out +=  "</div>\n"
        out +=  "<script>document.getElementById('ns_select').value = get_namespace();</script>"

    out +=  "</div>\n"

    # For all the menus in both panel sections.
    out +=  "<div class='all_menus'>\n"

    # For the left-hand panel sections (config, links).
    out +=  "<div class='narrow_menus'>\n"

    # Add the configuration menu components.
    out += __generate_config_menu(config["crds"])

    # Add the external links menu.
    out += __generate_external_links(config["externalLinks"])

    out +=  "</div>\n"

    # For the right-hand panel sections (pod, deployment, etc.).
    out +=  "<div class='wide_menus'>\n"

    # Add the scaling menu.
    out += __generate_scaling(config["scalables"], config["refreshInterval"])

    # Add the watched pods.
    out += __generate_watched_pods(config["watchedPods"], config["refreshInterval"])

    out +=  "</div>\n"
    out +=  "</div>\n"

    # Load the common and specific CSS style.
    out +=  "<style>\n"
    out +=  load_template("src/common_style.tmpl.css")
    out +=  load_template("src/home_page/style.tmpl.css")
    out +=  "</style>\n"

    # Add the page footing.
    out +=  "</body>\n"
    out +=  "</html>\n"

    # Try to open the output file and write results to it.
    try:
        with open(output_file, "w") as output:
            output.write(out)
    except Exception as e:
        error(f"Could not write the results to the {output_file} file.")


