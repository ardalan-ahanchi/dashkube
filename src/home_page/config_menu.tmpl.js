// This file defines the functionality that is needed to manage
// the configuration menu. Currently it implements the list 
// expansion on click functionality.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// Function to trigger the category for a specific button.
// the button parameter is expected to be "this" on onclick.
function trigger_category(button) { 
    // Activate the list.
    button.classList.toggle("active");

    // Get the category.
    var category = button.nextElementSibling;

    // Toggle the display based on the current state.
    category.style.display = 
            (category.style.display === "block" ? "none" : "block");
}
