// This file defines the operations that are valid for scaling.
//
// Author: Ardalan Ahanchi
// Date: January 2024

// This set will keep track of the current scalables list to refresh
// elements if needed.
currentScalables = new Set();

// Refresh the list every second.
const scalingIntId = setInterval(refresh_scaling, {{refresh_interval}});

// For the valid type/names to add to the list.
const validTypeNames = new Set([{{valid_type_names}}]);

// For the type/name to nickname, minReplicas, maxReplicas mapping.
const nicknameMap = {{{nickname_map}}};
const minReplicasMap = {{{min_replicas_map}}};
const maxReplicasMap = {{{max_replicas_map}}};

// Function to get the current scaling list and update the UI. It is called by a timer periodically.
async function refresh_scaling() {
    // Get the scalables info from the server.
    const response = await fetch(location.protocol + "//" + location.host + "/scale/apps/v1/All/namespaces/" + get_namespace(), {
        method: "GET",
    });

    // Check the response.
    if (response.ok) {
        response.text().then((value) => {
            // Parse the results.
            var parsed_unfiltered = JSON.parse(value);
            var parsed = []; 

            // Go through the unfiltered pods.
            for (let i = 0; i < parsed_unfiltered.length; i++) {
                // Filter based on the valid type/names.
                if (validTypeNames.has(parsed_unfiltered[i].type 
                        + "/" + parsed_unfiltered[i].name)) {
                    parsed.push(parsed_unfiltered[i]);
                }
            }

            // Check if we can skip refreshing based on parsed size.
            var skip_refresh = (parsed.length === currentScalables.size);

            // Check if all scalable information in the parsed are also in set.
            for (let i = 0; i < parsed.length; i++) {
                // If not, make sure we are refreshing.
                if (!currentScalables.has(parsed[i].type + parsed[i].name
                        + parsed[i].replicas + parsed[i].available_replicas)) {
                   skip_refresh = false;
                }
            }

//            // Set the state depending on number of items.
//            if (parsed.items.length == 0) {
//                curr_state.innerText = "No resources are currently configured.";
//                curr_state.style.display = "block";
//            } else {
//                curr_state.style.display = "none";
//            }

            // If we are not refreshing, exit for now.
            if (skip_refresh) {
                return;
            }

            // Get the template and list.
            var tmp = document.getElementById("scaling_list_element_template");
            var list = document.getElementById("scaling_list");

            // Clear the list and current scalables.
            list.innerHTML = "";
            currentScalables.clear();

            // Go through the items in the list and add an element cloned from
            // the template. Also get the resource info and set the text.
            for (let i = 0; i < parsed.length; i++) {
                var li = document.createElement("LI");
                var clone = tmp.content.cloneNode(true);

                // Set the value of the text fields accordingly.
                clone.getElementById("scaling_name").innerText = nicknameMap[parsed[i].type + "/" + parsed[i].name];
                clone.getElementById("scaling_type").innerText = "Type: " + parsed[i].type;
                clone.getElementById("scaling_available").innerText = "Available: " + parsed[i].available_replicas + "/" + parsed[i].replicas;

                // Set the value and min/max of the scaling text input.
                let scaleInput = clone.getElementById("scaling_target_count")
                scaleInput.value = parsed[i].replicas;
                scaleInput.min = minReplicasMap[parsed[i].type + "/" + parsed[i].name];
                scaleInput.max = maxReplicasMap[parsed[i].type + "/" + parsed[i].name];
                
                // Set the onclick for the scale button.
                clone.getElementById("scaling_scale_button").onclick = async function () {
                    // If the requested amount is different to current scale.
                    if (scaleInput.value != parsed[i].replicas) {
                        // Make the request to scale the pods.
                        const scale_response = await fetch(location.protocol +
                                "//" + location.host + "/scale/apps/v1/" + parsed[i].type +
                                "/namespaces/" + get_namespace() + "/" + parsed[i].name, {
                            method: "PUT",
                            headers: {
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify({"replicas": parseInt(scaleInput.value)})
                        });

                        // Do an alert to show the results.
                        scale_response.text().then((value) => {alert(value)});
                    }
                }

                // Set the onclick for the raw button.
                clone.getElementById("scaling_raw_button").onclick = function () {
                    window.location.href = "/raw/apps/v1/" + parsed[i].type + "/namespaces/" +  get_namespace() + "/" + parsed[i].name;
                }

                clone.id = "scaling_list_element_" + list.children.length;
                list.appendChild(clone);

                // Add this scalable info to the set of scalables.
                currentScalables.add(parsed[i].type + parsed[i].name
                        + parsed[i].replicas + parsed[i].available_replicas);
            }
        });
    } else {
        // Send an alert based on the server's response.
        response.text().then((value) => {alert(value)});
        if (typeof scalingIntId !== "undefined") {
            clearInterval(scalingIntId);
        }

//        // Set the state as an error state.
//        curr_state.style.display = "block";
//        curr_state.innerText = "Could not get the list of resources from the server.";
    }
}
