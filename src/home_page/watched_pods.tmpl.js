// This file defines the operations that are valid for watched pods list.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// This set will keep track of the current pods list to refresh
// elements if needed.
current_pods = new Set();

// Refresh the list every second.
const podsIntId = setInterval(refresh_pods, {{refresh_interval}});

// Function to get the current list of pods and update the UI it is called by a timer periodically.
async function refresh_pods() {
    // Get the pods info from the server.
    const response = await fetch(location.protocol + "//" + location.host + "/pods-info/" + get_namespace(), {
        method: "GET",
    });

    // Check the response.
    if (response.ok) {
        response.text().then((value) => {
            // Parse the results.
            var parsed_unfiltered = JSON.parse(value);
            var parsed = []; 

            // Go through the unfiltered pods.
            for (let i = 0; i < parsed_unfiltered.length; i++) {
                // Filter based on the pattern.
                if ({{ pods_pattern }}.test(parsed_unfiltered[i].name)) {
                    parsed.push(parsed_unfiltered[i]);
                }
            }

            // Check if we can skip refreshing based on parsed size.
            var skip_refresh = (parsed.length === current_pods.size);

            // Check if all pod information in the parsed are also in set.
            for (let i = 0; i < parsed.length; i++) {
                // If not, make sure we are refreshing.
                if (!current_pods.has(parsed[i].name + parsed[i].podStatus
                        + parsed[i].ready + parsed[i].restarts 
                        + parsed[i].startTime + parsed[i].nodeName)) {
                   skip_refresh = false;
                }
            }

//            // Set the state depending on number of items.
//            if (parsed.items.length == 0) {
//                curr_state.innerText = "No resources are currently configured.";
//                curr_state.style.display = "block";
//            } else {
//                curr_state.style.display = "none";
//            }

            // If we are not refreshing, exit for now.
            if (skip_refresh) {
                return;
            }

            // Get the template and list.
            var tmp = document.getElementById("pods_list_element_template");
            var list = document.getElementById("pods_list");

            // Clear the list and current pods.
            list.innerHTML = "";
            current_pods.clear();

            // Go through the items in the list and add an element cloned from
            // the template. Also get the resource info and set the text.
            for (let i = 0; i < parsed.length; i++) {
                var li = document.createElement("LI");
                var clone = tmp.content.cloneNode(true);

                // Set the value of the text fields accordingly.
                clone.getElementById("pod_name").innerText = parsed[i].name;
                clone.getElementById("pod_status").innerText = "Status: " + parsed[i].podStatus;
                clone.getElementById("pod_ready").innerText = "Ready: " + parsed[i].ready;
                clone.getElementById("pod_restarts").innerText = "Restarts: " + parsed[i].restarts;
                clone.getElementById("pod_node").innerText = "Node: " + parsed[i].nodeName;

                // Check if format can be converted to a date.
                var date_str = new Date(parsed[i].startTime).toLocaleString('en-US');
                if (date_str === "Invalid Date") {
                    date_str = parsed[i].startTime;
                }
                clone.getElementById("pod_start_time").innerText = "Start Time: " + date_str;

                // Set the onclick for the raw button.
                clone.getElementById("pod_raw").onclick = function () {
                    window.location.href = "/raw/core/v1/Pod/namespaces/" + get_namespace() + "/" + parsed[i].name
                }

                // Set the onclick for the logs button.
                clone.getElementById("pod_logs").onclick = function () {
                    window.location.href = "/logs/namespaces/" + get_namespace() + "/" + parsed[i].name
                }

                clone.id = "pods_list_element_" + list.children.length;
                list.appendChild(clone);

                // Add this pod info to the set of pods.
                current_pods.add(parsed[i].name + parsed[i].podStatus
                        + parsed[i].ready + parsed[i].restarts 
                        + parsed[i].startTime + parsed[i].nodeName);
            }
        });
    } else {
        // Send an allert based on the server's response.
        response.text().then((value) => {alert(value)});
        if (typeof podsIntId !== "undefined") {
            clearInterval(podsIntId);
        }

//        // Set the state as an error state.
//        curr_state.style.display = "block";
//        curr_state.innerText = "Could not get the list of resources from the server.";
    }
}
