// This file defines the operations that are valid for namespace.
//
// Author: Ardalan Ahanchi
// Date: January 2024

// Default namespace (will be the first in config).
const default_namespace = "{{default_namespace}}";

// Function to get the current namespace. Will return default if not set.
function get_namespace() {
    let ns = localStorage.getItem("namespace");
    
    // If namespace is not found in local storage, set it to default value.
    if (ns === null) {
        localStorage.setItem("namespace", default_namespace);
        ns = default_namespace;
    }

    return ns; 
}

// Simple wrapper to set the namespace to the new value.
function set_namespace(value) {
    localStorage.setItem("namespace", value);
}
