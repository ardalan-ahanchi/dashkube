# This file contains the program that runs when we want to autogenerate
# the user interface. It reads the configuration file that is passed to 
# it, validates it, and calls the appropriate generator programs to 
# create the final user interface.
# 
# Author: Ardalan Ahanchi
# Date: December 2023

import argparse
import yaml
import sys 
import schema
import time
import pathlib

from utils import *
from crd_form import crd_form_gen
from cr_manager import cr_manager_gen
from home_page import home_page_gen
from logs_page import logs_page_gen
from raw_page import raw_page_gen

# Schema that is used to verify the configuration file.
CONFIG_SCHEMA = schema.Schema({
    # Namespaces configured to work with the system.
    schema.Optional("namespaces", default=["default"]): [str],
    # Title to be used in the home screen.
    schema.Optional("title", default="Configuration Dashboard"): str,
    # Path to the logo that is shown in all screens.
    schema.Optional("logoPath", default="/static/dashkube_logo.svg"): str,
    # Interval to get updated information from the server in milliseconds.
    schema.Optional("refreshInterval", default=1000): int,
    # List of deployments that can be scaled.
    schema.Optional("scalables", default=[]): [
        {
            "type": schema.And(str, lambda t: t in (
                "Deployment",
                "ReplicaSet",
                "StatefulSet",
            )),
            "name": str,
            "nickname": str,
            schema.Optional("minReplicas", default=0): int,
            schema.Optional("maxReplicas", default=100): int,
        },
    ],
    # List of patterns for watched pods (for logs).
    schema.Optional("watchedPods", default=[]): [
        {
            # The pattern to look for and filter.
            "pattern": str,
        },
    ],
    # List of external links that can be shown in home screen.
    schema.Optional("externalLinks", default=[]): [
        {
            "nickname": str,
            "link": str,
            schema.Optional("description", default=""): str,
        },
    ],
    # List of CRDs to generate the UI for.
    "crds": [
        {
            "name": str,
            "nickname": str,
            schema.Optional("category", default=""): str,
            # For omitted top-level keys (such as status, metadata, etc.).
            schema.Optional("omittedKeys", default=[]): [str],
            # For omitted columns under additional printer columns.
            schema.Optional("omittedPrinterCols", default=[]): [str],
            # For string soft-references to other resources (CR kinds).
            schema.Optional("references", default=[]): [
                {
                    "field": str,   # Field that is the reference.
                    "to": str,      # CRD name that it is referencing.
                },
            ],
        },
    ],
})

# Function to validate and process the configuration file and return a parsed
# dictionary that is ready to use in the generators.
def __process_config(config_path: str) -> dict:
    # Try to open the config file and read the contents.
    try:
        with open(config_path) as config_file:
            config_raw = config_file.read()
    except Exception as e:
        error(f"Could not open the config file at {config_path}.")

    # Try to parse the config file and read the contents.
    try:
        config = yaml.safe_load(config_raw)
    except Exception as e:
        error(f"Could not parse the config file at {config_path}.")

    # Validate the config file based on the defined schema.
    try:
        config = CONFIG_SCHEMA.validate(config)
    except Exception as e:
        error(f"Passed configuration file not following the schema. {e}")

    # If there are no namespaces specified, add default.
    if len(config["namespaces"]) == 0:
        config["namespaces"] = ["default"]

    # Get the names of CRDs to nickname mapping and save them in a set for validation.
    crd_names: dict = {}
    for cfg in config["crds"]:
        crd_names[cfg["name"]] = cfg["nickname"]

    # Go through the CRDs that are defined.
    for crd_cfg in config["crds"]:
        # Validate references (if there are any), and add nickname.
        for ref in crd_cfg["references"]:
            # Validate the "field" field to make sure it is not explicitly indexed.
            if bool(re.search("\[.*[0-9+].*\]", ref["field"])):
                error(f"Referenced field for {crd_cfg['name']} can not contain index. Please use [:].")

            # Validate the "to" field to make sure it is a supported CRD.
            if ref["to"] not in crd_names:
                error(f"Invalid reference to {ref['to']} for the {crd_cfg['name']} CRD.")

            # Add nickname to the references section.
            ref["nickname"] = crd_names[ref["to"]]

    return config

# Function to generate all pages. This should be called on the first run, and
# anytime the configuration file is changed. This function can optionally
# run in daemon mode which will continously check and regenrate the UI.
def __generate_all(config: dict, output_dir: str, daemon: bool, daemon_int: int):
    # Generate the home page based on the required configuration.
    home_page_gen.generate(config, f"{output_dir}/index.html")

    # Generate the logs page and place at the desired place.
    logs_page_gen.generate(f"{output_dir}/logs.html", config["logoPath"])

    # Generate the raw page and place at the desired place.
    raw_page_gen.generate(f"{output_dir}/raw.html", config["logoPath"])

    # Log if starting the generator daemon.
    if daemon:
        print("Starting the UI generator in daemon mode.")

    # To keep the crd name to information map.
    crd_info: dict = {}

    # For the daemon, it will exit at the end of iteration 0 if not daemon.
    while True:
        # To keep track of if regen is required.
        crd_gen_needed: dict = {}

        # Go through the CRDs that are defined in config file, get their data
        # and check if regeneration is required. This needs to be done prior
        # to generation to handle references.
        for crd_cfg in config["crds"]:
            crd_name = crd_cfg["name"]

            # If CRD name is not present, initialize it.
            if crd_name not in crd_info:
                crd_info[crd_name] = CrdInfo(crd_name)

            # Sync the CRD and check if generation is needed.
            crd_gen_needed[crd_name] = crd_info[crd_name].sync_and_check(
                    set(crd_cfg["omittedPrinterCols"]))

        # Go through the CRD informations, and regenerate them if needed.
        for crd_cfg in config["crds"]:
            crd = crd_info[crd_cfg["name"]]

            # Check if generation is needed.
            if crd_gen_needed[crd.name]:
                # Log the generation event.
                print(f"Info: Generating UI elements for {crd.name}.")

                # Run the CRD form generator for this CRD after creating a name for output.
                crd_form_file: str = output_dir + "/" + crd.name.replace(".", "-") + "-crd-template.html"
                crd_form_gen.generate(crd.schema, f"{crd.group}/{crd.version}", crd.kind,
                        crd_cfg["nickname"], set(crd_cfg["omittedKeys"]), crd_cfg["references"],
                        crd_form_file, config["logoPath"], crd_info)

                # Run the CR manager generator for this CRD after creating a name for output.
                cr_manager_file: str = output_dir + "/" + crd.name.replace(".", "-") + "-cr-manager.html"
                cr_manager_gen.generate(crd, crd_cfg["nickname"], cr_manager_file,
                        config["refreshInterval"], config["logoPath"])

        # Check if this is a daemon.
        if daemon:
            # If it is, sleep for the interval specified.
            time.sleep(daemon_int)
        else:
            # Otherwise, simply exit this infinite loop.
            break


# Starting point to read the arugments, open required config, validate based on schema.
def main():
    # Define the command line arguments and parse them.
    parser = argparse.ArgumentParser(
        description="This programs reads the autogen config file and generates a UI for the CRDs that are on the cluster."
    )
    parser.add_argument(
        "-c",
        "--config-file",
        help="Path to the configuration YAML file. This defines CRDs, watched pods, and scalable deployments.",
        required=True,
    )
    parser.add_argument(
        "-o",
        "--output-dir",
        help="Path to the output directory that is used for all the generated files.",
        default="generated",
    )
    parser.add_argument(
        "-d",
        "--daemon-mode",
        help="Run the generator in daemon mode. It will generate as CRDs are updated.",
        action="store_true",
    )
    parser.add_argument(
        "-i",
        "--daemon-interval",
        help="Time to wait between checks for CRD updates for the daemon.",
        default=5,
    )

    args = parser.parse_args()

    # Create output directory if not present.
    try:
        pathlib.Path(args.output_dir).mkdir(parents=True, exist_ok=True)
    except Exception as e:
        error(f"Could not create the output directory at {args.output_dir}:\n{e}")

    # Process and parse the configuration file.
    config: dict = __process_config(args.config_file)

    # Call the generate all function since it is the first time generating.
    __generate_all(config, args.output_dir, args.daemon_mode, args.daemon_interval)


if __name__ == "__main__":
    main()
