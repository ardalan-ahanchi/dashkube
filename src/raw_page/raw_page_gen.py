# Script to generate a raw page which can display the raw
# resource from Kubernetes.
#
# Author: Ardalan Ahanchi
# Date: December 2023 

import argparse
import yaml
import copy
import pprint
import sys

sys.path.append('src')
from utils import *

# Function that is called by the main generator. It puts together
# all the files and adds the needed fields in runtime.
#
# Parameters
# `output_file`: Path to the output file. It will be created if not present.
# `logo_path`: Path to the logo file which is used for page header.
def generate(output_file: str, logo_path: str):
    # For the final output results.
    out: str = ""

    out +=  "<!DOCTYPE html>\n"
    out +=  "<html>\n"
    out +=  "<body onload='init_page()'>\n"

    # Add the logo and title with a link to index page.
    out +=  "<div class='page_header'>\n"
    out += f"<a href='/'><img src='{logo_path}' class='page_header_logo'></a>\n"
    out +=  "<h1 class='page_header_title'>Viewing {{kind}}/{{name}}</h1>\n"
    out +=  "</div>\n"

    # Add the paragraph to contain the raw resource.
    out +=  "<p id='contents' class='contents'></p>"

    # Load the JS file to load contents as needed. 
    out +=  "<script>\n"
    out +=  load_template("src/raw_page/raw_page.tmpl.js")
    out +=  "</script>\n"

    # Load the common and specific CSS style.
    out +=  "<style>\n"
    out +=  load_template("src/common_style.tmpl.css")
    out +=  load_template("src/raw_page/style.tmpl.css")
    out +=  "</style>\n"

    # Add the page footing.
    out +=  "</body>\n"
    out +=  "</html>\n"

    # Try to open the output file and write results to it.
    try:
        with open(output_file, "w") as output:
            output.write(out)
    except Exception as e:
        error(f"Could not write the results to the {output_file} file.")
