// This file defines the functionality needed to get the raw value for 
// all containers of a given resource.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// Initialize the page, get the raw resource.
async function init_page() {
    // Get the resource definition in the namespace.
    const response = await fetch(location.protocol + "//" + 
            location.host + "/resource/{{group}}/{{version}}/{{kind}}/namespaces/{{namespace}}/{{name}}", {
        method: "GET",
    });

    // Check the response.
    if (response.ok) {
        response.text().then(async (value) => {
            // Get the contents paragraph
            let contents = document.getElementById("contents");

            // Pretty print and set the value of the contents.
            contents.innerText = JSON.stringify(JSON.parse(value), null, 4);
        });
    // Alert the user if failed to get logs.
    } else {
        response.text().then((value) => {
            alert("Could not get the resouce from the server:\n" + value);
            history.back();
        });
    }
}
