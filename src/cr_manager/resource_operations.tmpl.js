// This file defines the operations that are valid for resources list.
// This includes vieweing, deleting, and editing elements.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// Define a function to view a raw resource with a given name from the cluster.
async function raw_resource(name) {
    window.location.href = "/raw/{{api_version}}/{{kind}}/namespaces/{{namespace}}/" + name;
}

// Define a function to delete a resource with a given name from the cluster.
async function delete_resource(name) {
    const response = await fetch(location.protocol + "//" + location.host + "/resource/{{api_version}}/{{kind}}/namespaces/{{namespace}}/" + name, {
        method: "DELETE",
    });
    response.text().then((value) => {
        alert(value);
        location.reload();
    });
}

// Define a function to edit a resource with a given name in the cluster.
async function edit_resource(name) {
    window.location.href = "/edit/{{plural}}/namespaces/{{namespace}}/" + name;
}

// Define a function to create a resource in the cluster.
async function create_resource() {
    window.location.href = "/create/{{plural}}/namespaces/{{namespace}}";
}
