# Script to generate a (mostly) static HTML page for a given CRD to get the 
# list of current CRs. It additionally allows deleting them and has a link
# to create a new CR.
#
# Author: Ardalan Ahanchi
# Date: December 2023 

import argparse
import yaml
import copy
import pprint
import sys

sys.path.append('src')
from utils import *

RES_NAME_KEYWORD: str = "RESOURCE_NAME"

# Main generation script. It requires some information about the CR and the schema,
# it then generates and returns the final output.
def __generate_manager(crd_info: CrdInfo, nickname: str,
        refresh_interval: int, logo_path: str) -> str:
    # For the final output.
    out: str = ""

    out +=  "<!DOCTYPE html>\n"
    out +=  "<html>\n"
    out +=  "<body onload='refresh_resources()'>\n"

    # Add the logo and title with a link to index page.
    out +=  "<div class='page_header'>\n"
    out += f"<a href='/'><img src='{logo_path}' class='page_header_logo'></a>\n"
    out += f"<h1 class='page_header_title'>Manage {nickname}</h1>\n"
    out +=  "</div>\n"

    # Add the state heading for when we don't have any resources.
    out +=  "<h2 id='current_state' class='current_state'></h2>\n"    

    # Add the list which will be used for the resources.
    out +=  "<ul id='resource_list' class='resource_list'></ul>\n"

    # To store the final printer columns HTML and JS.
    printer_cols_html: str = ""
    printer_cols_js: str = ""

    # Go through the printer columns, and create the HTML as needed.
    for col in crd_info.printer_cols:
        # Construct an ID to be used for the element.
        field_id: str = f"resource_element_col_{col['name'].lower()}"

        # Construct a title string if title is present.
        title: str = ""
        if "description" in col:
            description = col["description"].replace("\"", "&quot;")
            title = f"title=\"{description}\""
        
        # Add the HTML accordingly.
        printer_cols_html += f"<h3 id='{field_id}' class='wm_item_meta' {title}></h3>\n"
        
        # Find out how to convert the value of column depending on type.
        if col["type"] == "date":
            col_value = f"(new Date(parsed.items[i]{col['jsonPath']})).toLocaleString('en-US')"
        else:
            col_value = f"String(parsed.items[i]{col['jsonPath']})"

        # Add the code snippets to update field in JS.
        printer_cols_js += load_template("src/cr_manager/printer_cols.tmpl.js",
                field_id=field_id, name=col["name"], value=col_value)

    # Load the template for list element and populate resource name keyword.
    out +=  load_template("src/cr_manager/list_element.tmpl.html", 
                res_name_keyword=RES_NAME_KEYWORD,
                printer_cols=printer_cols_html)

    # Load the script needed for periodically refreshing the list. 
    out +=  "<script>\n"
    out +=  load_template("src/cr_manager/resource_refresh.tmpl.js", 
                api_version=f"{crd_info.group}/{crd_info.version}",
                kind=crd_info.kind,
                plural=crd_info.name,
                res_name_keyword=RES_NAME_KEYWORD, 
                refresh_interval=str(refresh_interval),
                printer_cols=printer_cols_js)
    out +=  "</script>\n"

    # Load templates that are used for resource list operations (raw/edit/delete).
    out +=  "<script>\n"
    out +=  load_template("src/cr_manager/resource_operations.tmpl.js",
                api_version=f"{crd_info.group}/{crd_info.version}",
                kind=crd_info.kind,
                plural=crd_info.name)
    out +=  "</script>\n"

    # Load common and specific styling to make the page pretty.
    out +=  "<style>\n"
    out +=  load_template("src/common_style.tmpl.css")
    out +=  load_template("src/cr_manager/style.tmpl.css")
    out +=  "</style>\n"

    # Add the create resource button.
    out += f"<button type='button' class='standalone_button' onclick='create_resource()'>Create</button>\n"

    out +=  "</form>\n"
    out +=  "</body>\n"
    out +=  "</html>\n"

    return out

# Function that is called by the main generator. It gets the required fields, 
# from the parsed CRD, and then calls the internal generate function
# with the correct parameters. It then writes the results to the output file.
#
# Parameters
# `crd_info`: The CRD info object parsed by main generator.
# `nickname`: Nickname that can be used for the CRD.
# `output_file`: Path to the output file. It will be created if not present.
# `refresh_interval`: Time between getting updated information from the server.
# `logo_path`: Path to the logo file used in the header.
# `printer_cols`: List of names of the printer columns to be used.
def generate(
        crd_info: CrdInfo,
        nickname: str, 
        output_file: str, 
        refresh_interval: int,
        logo_path: str):
    # Run the generate function with the parsed values.
    final_out: str = __generate_manager(crd_info, nickname,
            refresh_interval, logo_path)

    # Try to open the output file and write results to it.
    try:
        with open(output_file, "w") as output:
            output.write(final_out)
    except Exception as e:
        error(f"Could not write the results to the {output_file} file.")
