var {{field_id}} = clone.getElementById('{{field_id}}');
try {
    var {{field_id}}_value = "{{name}}: " + {{value}};
} catch (err) {
    var {{field_id}}_value = "Unknown";
}
{{field_id}}.innerText = {{field_id}}_value;
