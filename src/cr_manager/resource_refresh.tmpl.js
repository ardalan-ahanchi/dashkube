// This file defines the refreshing operation that is used to keep track of current resources.
// It defines a function and a timer to call it.
//
// Author: Ardalan Ahanchi
// Date: December 2023

// This set will keep track of resource UIDs plus resource version 
// to ensure we are only updating the list if needed.
current_uid_ver = new Set();

// Function which is called periodically by a timer to fetch resources.
// This function then overrides the contents of the list with the 
// recently fetched resources.
async function refresh_resources() {
    // Add apiVersion, kind, and metadata name fields.
    const response = await fetch(location.protocol + "//" + location.host + "/resource/{{api_version}}/{{kind}}/namespaces/{{namespace}}", {
        method: "GET",
    });


    // Get the state element to update it if needed.
    var curr_state = document.getElementById("current_state");
    if (response.ok) {
        response.text().then((value) => {
            // Parse the results.
            var parsed = JSON.parse(value);

            // Check if we can skip refreshing based on parsed items size.
            var skip_refresh = (parsed.items.length === current_uid_ver.size);

            // Check if all UID and version in the parsed are also in set.
            for (let i = 0; i < parsed.items.length; i++) {
                // If not, make sure we are refreshing.
                if (!current_uid_ver.has(parsed.items[i].metadata.uid + 
                        "/" + parsed.items[i].metadata.resourceVersion)) {
                   skip_refresh = false;
                }
            }

            // Set the state depending on number of items.
            if (parsed.items.length == 0) {
                curr_state.innerText = "No resources are currently configured.";
                curr_state.style.display = "block";
            } else {
                curr_state.style.display = "none";
            }

            // If we are not refreshing, exit for now.
            if (skip_refresh) {
                return;
            }

            // Get the template and list.
            var tmp = document.getElementById("resource_list_element_template");
            var list = document.getElementById("resource_list");
            
            // Clear the list and current UIDs.
            list.innerHTML = "";
            current_uid_ver.clear();

            // Go through the items in the list and add an element cloned from 
            // the template. Also get the resource name and replace it in the template.
            for (let i = 0; i < parsed.items.length; i++) {
                var res_name = parsed.items[i].metadata.name;
                var li = document.createElement("LI");
                var clone = tmp.content.cloneNode(true);

                // Add the additional printer columns as defined in the CRD.
{{printer_cols}}

                // Go through the elements with the keyword and ensure the 
                // resource name is correct.
                var all_items = clone.querySelectorAll("[id*={{res_name_keyword}}]")
                for (let j = 0; j < all_items.length; j++) {
                    all_items[j].outerHTML = all_items[j].outerHTML.replaceAll("{{res_name_keyword}}", res_name);
                }
                clone.id = "resource_list_element_" + list.children.length;
                list.appendChild(clone);
            
                // Add the current UID and version to the set.
                current_uid_ver.add(parsed.items[i].metadata.uid + "/" + 
                        parsed.items[i].metadata.resourceVersion);
            }
        });
    } else {
        // Send an allert based on the server's response.
        response.text().then((value) => {alert(value)});
        if (typeof intId !== "undefined") {
            clearInterval(intId);
        }

        // Set the state as an error state.
        curr_state.style.display = "block";
        curr_state.innerText = "Could not get the list of resources from the server.";
    }
}

// Refresh the list every second.
const intId = setInterval(refresh_resources, {{refresh_interval}});
