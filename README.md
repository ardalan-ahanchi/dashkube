# DashKube

Dashboard genreator for Kubernetes application. This project allows the users to automatically generate and serve a graphical user interface based on Kubernetes custom API definitions (CRDs). The generation is done based on a declarative (YAML) configuration file which describes which CRDs to look for and allows the users to personalize the user interface.

[[_TOC_]]

**This README is still under construction. Examples and more information will be added in the future.**

## Features

### Current

Please note that the project is currently in an alpha state, and bugs are expected. Currently the following features are present:

- Creating, Editing, or Deleting CRs for the specified CRDs.
- Monitoring printer columns for CRs.
- Viewing raw YAML for the desired CRs.
- Automatic fetching and form generation based on CRDs.
- On-the-fly updates to the forms if the CRDs are updated.
- CRD versioning. The storage version is always used for the GUI.
- Categorized configuration menu in the homepage to pick between CRDs.
- Multiple Kubernetes namespaces predefined in the configuration file.
- Monitoring pods based on a regex pattern.
- Fetching and subscribing to logs with the ability to select the desired container.
- Scaling deployments, replica sets, and stateful sets.
- Fetching raw YAML for the desired pods or scalables.
- Defining custom buttons for external links.
- Weak referencing. This is used where a field in a CRD is referencing the name of a differnet CR (from a different CRD).
- Helm chart that can be easily deployed in Kubernetes clusters.

### Missing

- Role based access controls (and authentication).
- Encryption.
- Ability to execute commands inside continaers.
- Server-side caching.
- Read-only mode.

## Build

- To create the docker image, please install docker on your machine, and run `make image`.
- To package the helm chart, please install helm on your machine, and run `make chart`.

## Artifacts

The build artifacts are published to the following sources:
- Docker image is published to `registry.gitlab.com/ardalan-ahanchi/dashkube`.
- Helm chart is published to `https://gitlab.com/api/v4/projects/55724069/packages/helm/stable`.

## Deployment

While this project can run in Kubernetes itself, it is flexible enough to run anywhere with access to the cluster. To deploy Dashkube in a Kubernetes cluster, you can follow this sequence:

- Define a values override for the helm chart with the image location, and the genreator configuration file. A minimal example is as follows:

```yaml
image:
  repository: "<Optional path to registry>"
  tag: "<Optional image tag to use. Will use latest if not specified>"

config:
  title: "<Optional title displayed in the homepage>"

  # More CRDs can be placed here if needed.
  crds:
  - name: "<Full CRD name (plural + group)>"
    nickname: "<Pretty name used in the UI for the CRD>"
    category: "<Optional category used to group CRDs>"
  
  # More namespaces can be listed here.
  namespaces:
  - <Kubernetes namespace>

  # Many more configurable fields are present. If interested, please look at 
  # the `CONFIG_SCHEMA` in the `src/generate.py" file.
```

- Install the helm chart in the cluster and provide the values overrides defined above by running the following command (please fill in the required information): `helm install dashkube --repo https://gitlab.com/api/v4/projects/55724069/packages/helm/stable dashkube --version <optional datkube version> -f <path to values overrides YAML file created>`
- Dashboard should be availabe on the IP address of the Kubernetes node.
