FROM python:slim-bookworm

# Create a work directory and copy content.
WORKDIR dashkube
COPY ./src ./src
COPY ./static ./static
COPY ./requirements.txt ./requirements.txt

# Install python dependencies.
RUN pip install --no-cache-dir -r requirements.txt

# By default hang and keep container running.
CMD ["tail", "-f", "/dev/null"]
