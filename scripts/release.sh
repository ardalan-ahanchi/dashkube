#! /bin/bash

# Script to do a release by building the chart and docker image,
# publishing it, and creating tag and a release in gitlab.
# The first parameter is the version to be published, second
# is the gitlab username, and third is the gitlab token.
# Please note that git, docker, and helm shall be installed.
#
# Author: Ardalan Ahanchi
# Date: March 18, 2024

# Check if the number of arguments is as expected.
if [ $# -ne 3 ]; then
    echo "Please pass in the required Paramaeters. The correct usage is:";
    echo "    ./release.sh <version> <gitlab username> <gitlab token>";
    exit 1;
fi

# To exit if anything fails.
set -e

# Define the used variables for readability.
PUBLISH_VERSION=$1
GITLAB_USER=$2
GITLAB_TOKEN=$3

# Create a tag for the release.
echo "Creating a tag for the release."
git tag v${PUBLISH_VERSION}
git push https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/ardalan-ahanchi/dashkube.git v${PUBLISH_VERSION}

# Package and post the helm chart with the published version.
echo "Building and releasing the helm chart."
helm package --version ${PUBLISH_VERSION} --app-version ${PUBLISH_VERSION} helm
curl --request POST --form "chart=@dashkube-${PUBLISH_VERSION}.tgz" --user ${GITLAB_USER}:${GITLAB_TOKEN} "https://gitlab.com/api/v4/projects/55724069/packages/helm/api/stable/charts"

# Login to the registry, build, and push the image to the registry.
echo "Building and releasing the docker image."
docker login -u="${GITLAB_USER}" -p "${GITLAB_TOKEN}" registry.gitlab.com
docker build -t registry.gitlab.com/ardalan-ahanchi/dashkube:v${PUBLISH_VERSION} -t registry.gitlab.com/ardalan-ahanchi/dashkube:latest .
docker push registry.gitlab.com/ardalan-ahanchi/dashkube:v${PUBLISH_VERSION}
docker push registry.gitlab.com/ardalan-ahanchi/dashkube:latest

# Create a gitlab release with the given tag.
curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/55724069/releases?tag_name=v${PUBLISH_VERSION}&ref=main"
echo

# Remove the helm chart locally.
rm "dashkube-${PUBLISH_VERSION}.tgz"

# Log the success message.
echo "Successfully released version ${PUBLISH_VERSION}."
